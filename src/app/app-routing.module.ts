import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnectComponent } from './connect/connect.component';
import { UrlShortenerComponent } from './url-shortener/url-shortener.component';


const routes: Routes = [
  {
    path: '',
    component: ConnectComponent
  },
  {
    path: 'url-shortener',
    component: UrlShortenerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
