import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { IShortenUrlReq } from '../dtos/url-shorten.req';
import { ShortenUrlService } from '../services/shorten-url.service';
import { SubscriptionBase } from '../base/subscription-base';
import { HttpErrorResponse } from '@angular/common/http';
import { IUrlShortenResponse } from '../dtos/url-shorten.response';

@Component({
  templateUrl: './url-shortener.component.html',
  styleUrls: ['./url-shortener.component.scss']
})
export class UrlShortenerComponent extends SubscriptionBase implements OnInit, OnDestroy {
  public form: FormGroup;
  public err = '';
  public providedUrl = '';

  get urlFC(): FormControl { return this.form.controls['url'] as FormControl; }

  constructor(
    formBuilder: FormBuilder,
    private router: Router,
    private shortenService: ShortenUrlService
  ) {
    super();

    this.form = formBuilder.group({
      url: ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  public onBackClick(): void {
    this.router.navigate([''], { skipLocationChange: true });
  }

  public shorten(): void {
    this.err = '';
    this.providedUrl = '';
    if (!this.form.invalid) {
      var req: IShortenUrlReq = {
        url: this.urlFC.value
      };
      this.shortenService.shortenUrl(req)
        .pipe(takeUntil(this.destroy$))
        .subscribe((response: IUrlShortenResponse) => {
          this.providedUrl = response.providedUrl;
        }, (err: HttpErrorResponse) => this.err = err.message);
    }
  }
  
  ngOnDestroy(): void {
    super.destroySubs();
  }
}
