export interface IUrlShortenResponse {
    providedUrl: string;
}