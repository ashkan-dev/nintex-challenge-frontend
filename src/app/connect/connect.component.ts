import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConnectService } from '../services/connect.service';
import { IAwsConnectReq } from '../dtos/aws-connect.req';
import { SubscriptionBase } from '../base/subscription-base';
import { takeUntil } from 'rxjs/operators';
import { errs } from '../enums';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.scss']
})
export class ConnectComponent extends SubscriptionBase implements OnInit, OnDestroy {
  public form: FormGroup;
  public err = '';

  get idFC(): FormControl { return this.form.controls['id'] as FormControl; }
  get secretFC(): FormControl { return this.form.controls['secret'] as FormControl; }

  constructor(
    formBuilder: FormBuilder,
    private ConnectService: ConnectService,
    private router: Router
  ) {
    super();

    this.form = formBuilder.group({
      id: ['', [Validators.required]],
      secret: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  public connect(): void {
    this.err = '';
    if (!this.form.invalid) {
      var req: IAwsConnectReq = {
        id: this.idFC.value,
        secret: this.secretFC.value
      };
      this.ConnectService.connectToAws(req)
        .pipe(takeUntil(this.destroy$))
        .subscribe((result: boolean) => {
          if (result) {
            this.router.navigate(['url-shortener'], { skipLocationChange: true });
          } else {
            this.err = errs.AwsNotConnect;
          }
        }, (err: HttpErrorResponse) => this.err = err.message);
    }
  }

  ngOnDestroy(): void {
    super.destroySubs();
  }
}
