import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAwsConnectReq } from '../dtos/aws-connect.req';
import { Observable } from 'rxjs';

@Injectable()
export class ConnectService {
  constructor(
    private httpClient: HttpClient
  ) { }

  public connectToAws(req: IAwsConnectReq): Observable<boolean> {
    return this.httpClient.post<boolean>('https://nintex-challenge-backend.azurewebsites.net/AwsOps/GetCredentials', req);
  }
}
