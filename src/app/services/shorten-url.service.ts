import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IShortenUrlReq } from '../dtos/url-shorten.req';
import { IUrlShortenResponse } from '../dtos/url-shorten.response';

@Injectable()
export class ShortenUrlService {
  constructor(
    private httpClient: HttpClient
  ) { }

  public shortenUrl(req: IShortenUrlReq): Observable<IUrlShortenResponse> {
    return this.httpClient.post<IUrlShortenResponse>('https://nintex-challenge-backend.azurewebsites.net/UrlOps/shortener', req);
  }
}
